import React, { Component } from "react";

export default class CartShoe extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
        {/* tao ham rendertbody xong goi render xuong duoi nay */}
        {/* LUU Y O TREN LA ARROW FUNCTION ,DAU NGOAC TRON NHO THEM VO KHONG THI GIAO DIEN NO KHONG RENDER :) FIX MAI K RAX DUMA  */}
      </table>
    );
  }
}
