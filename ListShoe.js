import React, { Component } from 'react'
import CartShoe from './CartShoe';
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
    renderListShoe = () => {
        // let result = this.props.shoeArr.map ((item) => { return <ItemShoe /> });
        // //result ban chat cua no la 1 cai array (vi map la array -> return cho minh 1 cai array)
        // //reactjs ho tro binding truc tiep tu array.
        // //
        // return result;
       // => thay vi minh phai tao mot cai bien trung gian(o day la let result, xong return result) thi minh co the return truc tiep luon:
       //Viet lai ham return truc tiep, sau do truyen vao cai ItemShoe du lieu
       return this.props.shoeArr.map((item) => { return <ItemShoe 
        handleViewDetail={this.props.handleChangeDetail}
        handleAddToCart ={this.props.handleAddToCart} 
        data={item} 
        /> });

    };
    
  render() {
    return (
      <div className='row'>{this.renderListShoe()}</div>
      //can dau ngoac tron de giao dien render vi o tren la function.
    )
  }
}
