import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let {image, name } = this.props.data 
    //boc du lieu ra, (data boc ra o day la data o file dataShoel minh goi name=data ben list shoe nen giu nguyen ten)
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button onClick={() => {this.props.handleAddToCart(this.props.data) }} className='btn btn-success mr-3'>Buy</button>
            {/* <button onClick={this.handleViewDetail} className='btn btn-primary'>Detail</button> sai nen code lai ben duoi */}
            {/* luc minh di dinh nghia cai handle thi ham no co params la value => onclick o day ham phai co arrow func, sau do truyen du lieu vo */ }
            <button onClick={() => {this.props.handleViewDetail(this.props.data) }} className='btn btn-primary'>Detail</button> 

            
          </div>
        </div>
      </div>
    );
  }
}
//muon no thay doi giao dien thi, render lai thi cho vao state
//Tuy nhien button dang o itemshoe con state thi o ex shoe shop
//=>State o dau thi minh se viet ham setState o do. 
//Button dang o item, ma tu th setState o exShoeShop no con di qua listshoe
//=> Phai dua du lieu tu exShoeShop toi ListShoe, xong tu ListShoe do no se truyen toi Item
