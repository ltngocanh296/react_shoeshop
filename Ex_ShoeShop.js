import React, { Component } from 'react'
import CartShoe from './CartShoe';
import { dataShoe } from './dataShoe'
import DetailShoe from './DetailShoe';
import ListShoe from './ListShoe';

export default class Ex_ShoeShop extends Component {
     //bthuong cgi lquan den thay doi du lieu thi bo vao state, 
    //Vi du them,xoa doi giay vao danh sach giay hien thi tren giao dien (push hay remove thi giao dien deu render lai, tang giam so luong...)
    state = {
        shoeArr: dataShoe,  //cai nay chi show list only
        detail: dataShoe[0], //cai nay la de khi click xem chi tiet no se show data ra giao dien
        cart: [], 
    };
    handleChangeDetail=(value)=>{ //handlechange o day la minh day ra ngoai state replace cai cart
        this.setState({detail:value});

    };
    handleAddToCart=(shoe)=> { //handle add to card thi minh cung day ra ngoai state nhung minh k replace ma minh push vao cai cart
        //=> de push vao thi minh clone cai cart co trong state sau do push vao, chu khong edit truc tiep vao state
        //=> clone thang hien tai la cart, de tao ra cai thang new chua gia tri hien tai, xong sau do push thi push vao cai th clone do, roi gan gia tri nguoc lai
        let cloneCart = [...this.state.cart];
        cloneCart.push(shoe);
        //cuoi cung set state lai:
        this.setState({cart:cloneCart});
     }
    //sau nay no se la array rong (shoeArr : []) - nghia la phai goi api no moi co du lieu, bai nay chua can.
    //sau do minh viet mot cai ham de render ra may cai item shoe

   

  render() {
    return (
      <div className='container'>
        {/*tren list shoe thi de cart */}
        <CartShoe cart={this.state.cart}/>
        <ListShoe shoeArr={this.state.shoeArr} 
        handleChangeDetail={this.handleChangeDetail}
        handleAddToCart={this.handleAddToCart}/>
        {/* sau do vao lai cai list, truyen tiep itemshoe */}
        {/* duoi list shoe xem chi tiet */}
        <DetailShoe details={this.state.detail}/>
      </div>
    )
  }
}

//bthuong co the render ngay o file nay, tuy nhien gio minh tach file ra de hoc. 