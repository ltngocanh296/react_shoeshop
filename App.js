import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction.jsx";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import DataBinding from "./DataBinding/DataBinding";
import DemoProps from "./DemoProps/DemoProps";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoState from "./DemoState/DemoState";
import Ex_Car from "./Ex_Car/Ex_Car";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* <DemoFunction></DemoFunction> */}
      {/* <Ex_Layout /> */}
      {/* <RenderWithMap /> */}
      {/* <DataBinding /> */}
      {/* <DemoProps /> */}
      {/* <DemoState /> */}
      {/* <Ex_Car /> */}
      <Ex_ShoeShop />
    </div>
  );
}

export default App;
